#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <errno.h>
#include <unistd.h>

// orgtable - read stdin and format input as org table
// set a split character (for now the same as org's, |)
// and an output style (for now org table only)
// and print the formatted table to stdout

// we'll simplify and allow only 256 rows
// later we should lift this limit

// we'll simplify and allow only 16 columns
// later we should lift this limit

size_t utf8len(char *s) {
        size_t len = 0;
        for (; *s; ++s) if ((*s & 0xc0) != 0x80) ++len;
        return len;
}

void free_array(char *data[][16], size_t row_count, size_t column_count) {
        for (size_t i = 0; i < row_count; i++) {
                for (size_t j = 0; j < column_count; j++) {
                        if (data[i][j]) {
                                free(data[i][j]);
                        }
                }
        }
}

void init_array(char *data[][16]) {
        for (size_t i = 0; i < 256; i++) {
                for (size_t j = 0; j < 16; j++) {
                        data[i][j] = NULL;
                }
        }
}

char* space() {
        char *str = malloc(sizeof(char) * 2);
        str[0] = ' ';
        str[1] = '\0';
        return str;
}

void fill_blanks(char *data[][16], size_t row_count, size_t column_count) {
        for (size_t i = 0; i < row_count; i++) {
                for (size_t j = 0; j < column_count; j++) {
                        if (!data[i][j]) {
                                data[i][j] = space();
                        }
                }
        }
}

void print_breaking_line (size_t *column_sizes, size_t column_count) {
        printf("|");
        for (size_t i = 0; i < column_sizes[0] + 2; i++)
                printf("-");
        for (size_t j = 1; j < column_count; j++) {
                printf("+");
                for (size_t i = 0; i < column_sizes[j] + 2; i++) {
                        printf("-");
                }
        }
        printf("|\n");
}

int run(int add_title, int add_footer) {
        char delim = '|';
        char *str, *strnext; size_t str_size, utf8_size;
        char *data[256][16];
        size_t column_sizes[16] = {0};
        size_t column_numerics[16] = {0};
        char *line = NULL, *start, *at, *end;
        size_t len = 0;
        size_t column = 0, column_count = 0, row = 0, row_count = 0;
        int width;
        int reached_eof = 0;
        setlocale(LC_ALL, ""); // sets up UTF8
        init_array(data);
        while (getline(&line, &len, stdin) != -1) {
                if (row >= 256) {
                        fprintf(stderr, "Maximum 256 rows supported\n");
                        free(line);
                        free_array(data, row, column_count);
                        exit(1);
                }
                start = line;
                // empty line means finish this table and start a new one
                if (*start == '\n') {
                        reached_eof = 1;
                        break;
                }
                // treat first delimiter as table border
                if (*start == delim) start++;
                // skip first spaces
                while (*start == ' ') start++;
                at = start;
                while (*at) {
                        if (*at == delim || (*at == '\n' && at != start) ) {
                                if (column >= 16) {
                                        fprintf(stderr, "Maximum 16 columns supported\n");
                                        free(line);
                                        free_array(data, row+1, column);
                                        exit(1);
                                }
                                // decrement by number of trailing spaces
                                end = at - 1;
                                while (*end == ' ') {
                                        end--;
                                }
                                if (start > end) {
                                        data[row][column] = space();
                                } else {
                                        str_size = end - start + 2;
                                        // ^ +1 for the subtraction because the pointers point to start and end
                                        //   mySpecialStringy
                                        //     ^start   ^end  ==> SpecialStr
                                        // +1 more for the finishing null byte '\0'
                                        str = malloc(sizeof(char) * str_size);
                                        strncpy(str, start, str_size - 1);
                                        str[str_size-1] = '\0';
                                        data[row][column] = str;
                                        utf8_size = utf8len(str);
                                        // set column size to max
                                        if (column_sizes[column] < utf8_size)
                                                column_sizes[column] = utf8_size;
                                        errno = 0;
                                        strtod(str, &strnext);
                                        if (!errno && !*strnext) {
                                                // conversion worked
                                                column_numerics[column]++;
                                        }
                                }
                                /* printf("%zu\n", column_sizes[column]); */
                                column++;
                                at++;
                                // skip first spaces
                                while (*at == ' ') at++;
                                start = at;
                        } else {
                                at++;
                        }
                }
                free(line);
                line = NULL;
                column_count = column;
                len = 0;
                column = 0;
                row++;
        }
        free(line);
        row_count = row;
        fill_blanks(data, row_count, column_count);
        for (row = 0; row < row_count; row++) {
                if (((row == 1 && add_title) ||
                     (row == row_count - 1 && add_footer)) &&
                    row_count > 1) {
                        print_breaking_line(column_sizes, column_count);
                }
                printf("|");
                for (column = 0; column < column_count; column++) {
                        // the width specifier needs to make corrections for utf8 byte offsets
                        // it assures the given number of BYTEs are printed
                        // so %-2s for á will not print a space because it is 2 bytes already
                        // ... so the width needs to be:
                        // if we want e.g. a column of 10
                        // and we have ááá as input --> 6 bytes
                        // and we need 7 more spaces...
                        // that is 13 bytes...
                        // so, column_size + strlen - utf8len
                        str = data[row][column];
                        str_size = strlen(str);
                        utf8_size = utf8len(str);
                        width = (int) (column_sizes[column] + str_size - utf8_size);
                        if (column_numerics[column] * 2 >= row_count) { // numeric
                                printf(" %*s |", width, str);
                        } else {
                                printf(" %-*s |", width, str);
                        }
                }
                printf("\n");
        }
        free_array(data, row_count, column_count);
        return reached_eof;
}

int main(int argc, char *argv[]) {
        char opt;
        int add_title= 0, add_footer = 0;
        while ((opt = getopt(argc, argv, "TF")) != -1) {
                switch (opt) {
                case 't':
                        add_title= 1;
                        break;
                case 'f':
                        add_footer = 1;
                        break;
                default:
                        fprintf(stderr, "Usage: %s [-t] [-f]\n", argv[0]);
                        exit(EXIT_FAILURE);
                }
        }
        while(run(add_title, add_footer)) printf("\n");
}
